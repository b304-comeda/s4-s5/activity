public class Contact {

    public String name;
    public String contactNumber1;
    public String contactNumber2;
    public String address1;
    public String address2;

    // default constructor ===============================
    public Contact() {}

    // parameterized constructor ===============================
    public Contact(String name, String contactNumber1, String contactNumber2, String address1, String address2) {
        this.name = name;
        this.contactNumber1 = contactNumber1;
        this.contactNumber2 = contactNumber2;
        this.address1 = address1;
        this.address2 = address2;
    }

    // Getters ===============================

    public String getName() {
        return name;
    }

    public String getContactNumber1() {
        return contactNumber1;
    }

    public String getContactNumber2() {
        return contactNumber2;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }


    // Setters ===============================

    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumber1(String contactNumber1) {
        this.contactNumber1 = contactNumber1;
    }

    public void setContactNumber2(String contactNumber2) {
        this.contactNumber2 = contactNumber2;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }
}
