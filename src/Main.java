
public class Main {
    public static void main(String[] args) {

        // instantiating phonebook from Phonebook class
        Phonebook phonebook = new Phonebook();

        // instantiating 2 contacts from the Contact class
        Contact contact1 = new Contact(
                "John Doe",
                "+639152468596",
                "+639228547963",
                "Quezon City",
                "Makati City"
        );


        Contact contact2 = new Contact(
                "Jane Doe",
                "+639162148573",
                "+639173698541",
                "Caloocan City",
                "Pasay City"
        );

        // adding both contacts to phonebook object
        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {
            phonebook.getContacts().forEach(contact -> {
                System.out.println(contact.getName());
                System.out.println("-------------------");
                System.out.println(contact.getName() + " has the following registered numbers: \n" + contact.getContactNumber1() + "\n" + contact.getContactNumber2());
                System.out.println("-----------------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println("my home in " + contact.getAddress1()+ "\n" + "my office in " + contact.getAddress2());
                System.out.println("===================================");
                System.out.println();
            });
        }



    }
}