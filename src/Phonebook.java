import java.util.ArrayList;

public class Phonebook {

    public ArrayList<Contact> contacts = new ArrayList<>();
//    public ArrayList<Contact> contacts;

    // default constructor ==================================
    public Phonebook(){

    }

    // parameterized constructor ==================================
    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    // Getters and Setters ====================================
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }


}
